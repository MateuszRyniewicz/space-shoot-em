﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour
{

    public static GameManager instance;

    public int currentLive;

    public int timeToRespown;

    public int currentScore;

    private int highScore = 500;

    public bool levelEnding;

    private int levelScore;

    public float timeToNextLevel;
    public string nextLevel;

    public bool canPaused;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        canPaused = true;
        Time.timeScale = 1f;

        currentLive = PlayerPrefs.GetInt("currentLive");

        UiManager.instance.liveText.text = " X " + currentLive;
        

        PlayerPrefs.GetInt("highScore");
        UiManager.instance.hiScoreText.text = "Hi-Score: " + highScore;

        currentScore = PlayerPrefs.GetInt("CurrentScore");
        UiManager.instance.scoreText.text = "Score:" + currentScore;
    }

    // Update is called once per frame
    void Update()
    {
        if (levelEnding)
        {
            PlayerController.instance.transform.position += new Vector3(PlayerController.instance.boostSpeed * Time.deltaTime, 0f, 0f);
        }

        if (Input.GetKeyDown(KeyCode.Escape) && canPaused)
        {
            PauseUnPause();
        }
    }

    public void KillPlayer()
    {
        currentLive--;
        UiManager.instance.liveText.text = " X " + currentLive;

        if (currentLive > 0)
        {
            StartCoroutine(RespownCo());
        }
        else
        {
            canPaused = false;

            UiManager.instance.gameOverScreen.gameObject.SetActive(true);
            WaveManager.instance.canSpawnWaves = false;

            MusicController.instance.PlayGameOver();

            PlayerPrefs.SetInt("highScore", highScore);
        }
    }

    public IEnumerator RespownCo()
    {
        yield return new WaitForSeconds(timeToRespown);
        HealthManager.instance.Respown();

        WaveManager.instance.ContinueSpawing();
    }

    public void addScore(int valeScore)
    {
        currentScore += valeScore;
        levelScore += valeScore;
        UiManager.instance.scoreText.text = "Score: " + currentScore;

        if (currentScore > highScore)
        {
            highScore = currentScore;
            UiManager.instance.hiScoreText.text = "Hi-Score: " + highScore;

           // PlayerPrefs.SetInt("highScore", highScore);

        }
    }

    public IEnumerator EndLevelCo()
    {
        canPaused = false;
        
        UiManager.instance.levelEndScreen.gameObject.SetActive(true);
        PlayerController.instance.stopMovment = true;
        levelEnding = true;

        MusicController.instance.StopMusic();
        MusicController.instance.victoryMusic.Play();

        yield return new WaitForSeconds(0.5f);

        UiManager.instance.endLevelScore.text = "Level Score: " + levelScore;
        UiManager.instance.endLevelScore.gameObject.SetActive(true);

        yield return new WaitForSeconds(0.5f);

        PlayerPrefs.SetInt("CurrentScore", currentScore);
        UiManager.instance.endCurrentLevelScore.text = "Total Score: " + currentScore;
        UiManager.instance.endCurrentLevelScore.gameObject.SetActive(true);

        if (currentScore >= highScore)
        {
            UiManager.instance.highScore.SetActive(true);
        }

        PlayerPrefs.SetInt("highScore", highScore);
        PlayerPrefs.SetInt("currentLive", currentLive);

        yield return new WaitForSeconds(timeToNextLevel);
        SceneManager.LoadScene(nextLevel);

    }

    public void PauseUnPause()
    {
        if (UiManager.instance.pausedScreen.activeInHierarchy)
        {
            UiManager.instance.pausedScreen.SetActive(false);
            PlayerController.instance.stopMovment = false;

            Time.timeScale = 1f;
        }
        else
        {
            UiManager.instance.pausedScreen.SetActive(true);
            PlayerController.instance.stopMovment = true;

            Time.timeScale = 0f;
        }
    }
}
