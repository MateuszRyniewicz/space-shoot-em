﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossManager : MonoBehaviour
{

    public static BossManager instance;

    public int currentHealth;
    public string nameBoss;

    //  public BattleShot [] shotsToFires;
    public BattlePharse [] pharse;
    public int currentPhase;
    public Animator bossAnim;

    public GameObject endExplosion;
    public bool battleEnding;
    public float timeToExplosionEnd;
    public float waitToEndLevel;

    public Transform theBoss;

    public int scoreVale = 10000;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        UiManager.instance.nameBoss.text = nameBoss;
        UiManager.instance.bossHealthSlider.maxValue = currentHealth;
        UiManager.instance.bossHealthSlider.value = currentHealth;
        UiManager.instance.bossHealthSlider.gameObject.SetActive(true);

        MusicController.instance.PlayBoss();
    }

    // Update is called once per frame
    void Update()
    {

        if (!battleEnding)
        {


            if (currentHealth <= pharse[currentPhase].healthToEndPhase)
            {
                pharse[currentPhase].removeAtPhaseEnd.gameObject.SetActive(false);

                Instantiate(pharse[currentPhase].addAtPhaseEnd, pharse[currentPhase].newSpawnPoint.position, pharse[currentPhase].newSpawnPoint.rotation);

                currentPhase++;

                bossAnim.SetInteger("Phase", currentPhase + 1);
            }
            else
            {
                for (int i = 0; i < pharse[currentPhase].phaseShots.Length; i++)
                {
                    pharse[currentPhase].phaseShots[i].counterShot -= Time.deltaTime;

                    if (pharse[currentPhase].phaseShots[i].counterShot <= 0)
                    {
                        pharse[currentPhase].phaseShots[i].counterShot = pharse[currentPhase].phaseShots[i].timeBeetwenShot;

                        Instantiate(pharse[currentPhase].phaseShots[i].theShot, pharse[currentPhase].phaseShots[i].firePoint.transform.position, pharse[currentPhase].phaseShots[i].firePoint.transform.rotation);
                    }
                }
            }
        }
    }
   

    public void HurtBoss()
    {
        currentHealth--;
        UiManager.instance.bossHealthSlider.value = currentHealth;
        if (currentHealth <= 0 && !battleEnding)
        {
            /*
             *
             * Destroy(gameObject);

              UiManager.instance.bossHealthSlider.gameObject.SetActive(false);

              *
              *  
              */

            battleEnding = true;
            StartCoroutine(EndBattleCo());

        }
    }

    public IEnumerator EndBattleCo()
    {
        UiManager.instance.bossHealthSlider.gameObject.SetActive(false);
        Instantiate(endExplosion, theBoss.position, theBoss.rotation);
        bossAnim.enabled = false;
        GameManager.instance.addScore(scoreVale);

        yield return new WaitForSeconds(timeToExplosionEnd);

        theBoss.gameObject.SetActive(false);

        yield return new WaitForSeconds(waitToEndLevel);

        StartCoroutine(GameManager.instance.EndLevelCo());
    }
}

[System.Serializable]
public class BattleShot
{
    public GameObject theShot;
    public float timeBeetwenShot;
    public float counterShot;
    public Transform firePoint;
}

[System.Serializable]
public class BattlePharse
{
    public BattleShot[] phaseShots;
    public int healthToEndPhase;
    public GameObject removeAtPhaseEnd;
    public GameObject addAtPhaseEnd;
    public Transform newSpawnPoint;
}
