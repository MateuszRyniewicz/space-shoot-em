﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthManager : MonoBehaviour
{

    public static HealthManager instance;

    public int currentHealth;
    public int maxHealth;

    public GameObject deathEffect;

    public float invincibleLength = 2f;
    public float invinCounter;

    public SpriteRenderer theSr;

    public int shieldPower;
    public int shieldPowerMax=2;
    public GameObject shiled;


    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
        UiManager.instance.healthSlider.value = currentHealth;
        UiManager.instance.healthSlider.maxValue = maxHealth;

        UiManager.instance.shieldSlider.value = shieldPower;
        UiManager.instance.shieldSlider.maxValue = shieldPowerMax;

        
    }

    // Update is called once per frame
    void Update()
    {
        if (invinCounter >= 0)
        {
            invinCounter -= Time.deltaTime;
        }

        if (invinCounter < 0)
        {
            theSr.color = new Color(theSr.color.b, theSr.color.g, theSr.color.b, 1f);
        }
    }

    public void HurtPlayer()
    {
        if (invinCounter <= 0)
        {
            if (shiled.activeInHierarchy)
            {
                shieldPower--;
                if (shieldPower <= 0)
                {
                    shiled.gameObject.SetActive(false);
                }

                UiManager.instance.shieldSlider.value = shieldPower;

            }
            else
            {



                currentHealth--;
                UiManager.instance.healthSlider.value = currentHealth;
                PlayerController.instance.dubbleShotActive = false;

                if (currentHealth <= 0)
                {

                    Instantiate(deathEffect, transform.position, transform.rotation);

                    gameObject.SetActive(false);

                    GameManager.instance.KillPlayer();

                    WaveManager.instance.canSpawnWaves = false;

                }
            }
        }
    }

    public void Respown()
    {
        gameObject.SetActive(true);
        currentHealth = maxHealth;

        UiManager.instance.healthSlider.value = currentHealth;

        invinCounter = invincibleLength;

        theSr.color = new Color(theSr.color.r, theSr.color.g, theSr.color.b, 0.5f);
    }

    public void ActiveShield()
    {
        shieldPower = shieldPowerMax;
        shiled.gameObject.SetActive(true);

        UiManager.instance.shieldSlider.value = shieldPower;
    }
}
