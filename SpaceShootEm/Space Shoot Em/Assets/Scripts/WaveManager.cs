﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveManager : MonoBehaviour
{
    public static WaveManager instance;

    public WaveObjects[] waves;
    public int currentWave;

    public float timeSpawnWave;

    public bool canSpawnWaves;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        timeSpawnWave = waves[0].timeToSpawn;
    }

    // Update is called once per frame
    void Update()
    {
        if (canSpawnWaves)
        {


            timeSpawnWave -= Time.deltaTime;
            if (timeSpawnWave <= 0)
            {
                Instantiate(waves[currentWave].theWave, transform.position, transform.rotation);


                if (currentWave < waves.Length-1)
                {

                    currentWave++;

                    timeSpawnWave = waves[currentWave].timeToSpawn; 
                }
                else
                {
                    canSpawnWaves = false;
                }
            }
        }
    }

    public void ContinueSpawing()
    {
        if(currentWave <=waves.Length && timeSpawnWave > 0)
        {
            canSpawnWaves = true;
        }
    }
}

[System.Serializable]
public class WaveObjects
{
    public float timeToSpawn;
    public EnemyWaves theWave;
}

