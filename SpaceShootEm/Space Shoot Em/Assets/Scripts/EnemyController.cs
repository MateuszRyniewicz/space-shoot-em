﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{

    public float moveSpeed = 3f;
    public Vector2 moveDirection;

    public bool shouldChangeDirection;
    public float changeXDirection;
    public Vector2 changedDirection;

    public GameObject shotEnemy;
    public Transform firePoint;
    public float timeBetweenShot;
    private float shotCounter;

    public bool canShot;
    private bool allowShooting;


    public GameObject deathEffect;
    public int curretHealth;


    public GameObject[] powerUps;

    public int chanceToDrop = 15;
    // Start is called before the first frame update
    void Start()
    {
        shotCounter = timeBetweenShot;
    }

    // Update is called once per frame
    void Update()
    {
        //  transform.position -= new Vector3(moveSpeed * Time.deltaTime, 0, 0);

        if (!shouldChangeDirection)
        {

            transform.position += new Vector3(moveDirection.x * moveSpeed * Time.deltaTime, moveDirection.y * moveSpeed * Time.deltaTime);

        }
        else
        {
            if (transform.position.x > changeXDirection)
            {
                transform.position += new Vector3(moveDirection.x * moveSpeed * Time.deltaTime, moveDirection.y * moveSpeed * Time.deltaTime);
            }
            else
            {
                transform.position += new Vector3(changedDirection.x * moveSpeed * Time.deltaTime, changedDirection.y * moveSpeed * Time.deltaTime);
            }
        }


        if (allowShooting)
        {


            shotCounter -= Time.deltaTime;

            if (shotCounter <= 0)
            {
                shotCounter = timeBetweenShot;
                Instantiate(shotEnemy, transform.position, transform.rotation);
            }
        }
    }


    public void OnBecameInvisible()
    {
        Destroy(gameObject);   
    }

    public void OnBecameVisible()
    {
        if (canShot)
        {
            allowShooting = true;
        }
    }

    public void HurtEnemy()
    {
        curretHealth--;

        if (curretHealth <= 0)
        {
            GameManager.instance.addScore(100);

            int randomChance = Random.Range(0, 100);

            if (randomChance < chanceToDrop)
            {
                int randomPickUps = Random.Range(0, powerUps.Length);
                Instantiate(powerUps[randomPickUps], transform.position, transform.rotation);

            }



            Instantiate(deathEffect, transform.position, transform.rotation);
            Destroy(gameObject);
        }
    }
}
