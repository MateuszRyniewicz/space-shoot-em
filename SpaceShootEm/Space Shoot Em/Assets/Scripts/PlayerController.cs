﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public static PlayerController instance;


    private void Awake()
    {
        instance = this;
    }


    public float moveSpeed = 5f;
    private Rigidbody2D rb;

    public Transform buttonTopRightlimit, buttonDownLeftLimit;

    public Transform firePoint;
    public GameObject shotPlayer;

    public float timeBetweenShot = 0.2f;
    private float shotCounter;

    private float normalSpeed;
    public float boostSpeed;
    public float boostLenght;
    private float boostCounter;

    public bool dubbleShotActive;
    public float dubbleShotOffset;

    public bool stopMovment;
    
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();

        normalSpeed = moveSpeed;
       
    }

    // Update is called once per frame
    void Update()
    {
        if (!stopMovment)
        {


            rb.velocity = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")) * moveSpeed * Time.deltaTime;

            transform.position = new Vector3(Mathf.Clamp(transform.position.x, buttonDownLeftLimit.transform.position.x, buttonTopRightlimit.position.x), Mathf.Clamp(transform.position.y, buttonDownLeftLimit.position.y, buttonTopRightlimit.position.y), transform.position.z);


            if (Input.GetButtonDown("Fire1"))
            {
                if (!dubbleShotActive)
                {

                    Instantiate(shotPlayer, firePoint.position, firePoint.rotation);
                }
                else
                {
                    Instantiate(shotPlayer, firePoint.position + new Vector3(0, dubbleShotOffset, 0), firePoint.rotation);
                    Instantiate(shotPlayer, firePoint.position - new Vector3(0, dubbleShotOffset, 0), firePoint.rotation);
                }

                shotCounter = timeBetweenShot;
            }

            if (Input.GetButton("Fire1"))
            {
                shotCounter -= Time.deltaTime;

                if (shotCounter <= 0)
                {
                    if (!dubbleShotActive)
                    {
                        Instantiate(shotPlayer, firePoint.position, firePoint.rotation);

                    }
                    else
                    {
                        Instantiate(shotPlayer, firePoint.position + new Vector3(0, dubbleShotOffset, 0), firePoint.rotation);
                        Instantiate(shotPlayer, firePoint.position - new Vector3(0, dubbleShotOffset, 0), firePoint.rotation);
                    }
                    shotCounter = timeBetweenShot;
                }

            }

            if (boostCounter > 0)
            {
                boostCounter -= Time.deltaTime;

                if (boostCounter < 0)
                {
                    moveSpeed = normalSpeed;
                }
            }

        }
        else
        {
            rb.velocity = Vector2.zero;
        }
    }


    public void ActivateBoostSpeed()
    {
        moveSpeed = boostSpeed;
        boostCounter = boostLenght;
    }

   
}
