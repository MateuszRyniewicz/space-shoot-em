﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UiManager : MonoBehaviour
{
    public static UiManager instance;

    public GameObject gameOverScreen;
    public GameObject levelEndScreen;

    public Text liveText;

    public Slider healthSlider,shieldSlider;

    public Text scoreText, hiScoreText;

    public Text endLevelScore, endCurrentLevelScore;
    public GameObject highScore;

    public GameObject pausedScreen;

    public string mainMenu;

    public Slider bossHealthSlider;
    public Text nameBoss;


    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);

        Time.timeScale = 1f;
    }

    public void MainMenu()
    {
        SceneManager.LoadScene(mainMenu);
        Time.timeScale = 1f;
    }

    public void Resume()
    {
        GameManager.instance.PauseUnPause();
    }
}
