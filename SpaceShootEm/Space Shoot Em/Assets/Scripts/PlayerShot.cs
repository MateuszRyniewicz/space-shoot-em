﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShot : MonoBehaviour
{
    public float shotSpeed=7;

    public GameObject impactEffect;

    public GameObject explosionEffect;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += new Vector3(shotSpeed * Time.deltaTime, 0f, 0f);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        Instantiate(impactEffect, transform.position, transform.rotation);

        if(other.tag == "Space Object")
        {

            GameManager.instance.addScore(50);

            Instantiate(explosionEffect, other.transform.position, other.transform.rotation);
            Destroy(other.gameObject);
        }

        if (other.tag == "Enemy")
        {
            other.GetComponent<EnemyController>().HurtEnemy();
        }

        if (other.tag == "Boss")
        {
            BossManager.instance.HurtBoss();
        }


        Destroy(gameObject);
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
